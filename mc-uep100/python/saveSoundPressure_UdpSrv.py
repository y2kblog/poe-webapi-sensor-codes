############
# saveSoundPressure_UdpSrv.py v1.0
# 
# Copyright (c) 2020 Y2Kb
# 
# Released under the MIT license.
# see https://opensource.org/licenses/MIT
############
## Description: Save time-series sound pressure data (endpoint: /raw) obtained from PoE-WebAPI-Ultrasonic-Sensor to a file
## Command example: 'python saveSoundPressure_UdpSrv.py 60000'
## 1st argument: port number (default:50000)
## Python version: 3.8.1~
## Caution: 
##   - Since asynchronous I/O is used, the obtained value will be wrong if the order of received packets is changed.
##   - Since all values while receiving sound pressure data are held as array-type variables, long reception time may run out of memory.
import sys
import asyncio
import json
import os
import pathlib
from datetime import datetime
from array import array

## Packages for myFunc
## Package install command: `pip install matplotlib numpy scipy wavio obspy`
import math
import wave
import struct
import matplotlib.pyplot as plt # pip install matplotlib OR sudo apt-get install python3-matplotlib OR python3-matplotlib
import numpy as np
import scipy.signal
import wavio
from obspy.signal.tf_misfit import cwt


## Constant value
SAVE_DIR = 'data/'


class UdpServer:
    ## `myFunc` is called when data reception is completed.
    def myFunc(self, frqHz):
        ## Write your code. Data(unit: Pa) is held in `self.soundDataArr`

        ## High-pass filter
        MIN_FRQ = 10.0      # cutoff frequency [Hz]
        P0_Pa = 0.000020    # reference value p0 (0dB SPL = 20uPa)
        hpf1 = scipy.signal.firwin(numtaps=201, cutoff=MIN_FRQ, fs = frqHz, pass_zero=False)
        self.soundDataArr = scipy.signal.lfilter(hpf1, 1, self.soundDataArr)

        ## ex.1 save wave file
        ## 16bit WAVE
        rangePa = np.max(np.abs(self.soundDataArr)) * 2     # set max range of wave file
        rangePa = rangePa if rangePa < 20 else 20
        waveDataArr = array('h')
        waveDataArr = [int((20 if x > 20 else (-20 if x < -20 else x)) / rangePa * 32767.0) for x in self.soundDataArr]
        binwave = struct.pack(f'{len(waveDataArr)}h', *waveDataArr)
        strBuf = SAVE_DIR + self.filename + "_16b.wav"
        w = wave.Wave_write(strBuf)
        p = (1, 2, frqHz, len(binwave), 'NONE', 'not compressed')
        w.setparams(p)
        w.writeframes(binwave)
        w.close()
        print('Saved to "' + strBuf + '"')

        ## 24bit-wave
        # rangePa = np.max(np.abs(self.soundDataArr)) * 2     # set max range of wave file
        # rangePa = rangePa if rangePa < 20 else 20
        # waveDataArr = array('f')
        # waveDataArr = [(20.0 if x > 20.0 else (-20.0 if x < -20.0 else x)) / rangePa * 8388607.0 for x in self.soundDataArr]
        # strBuf = SAVE_DIR + self.filename + "_24b.wav"
        # wavio.write(strBuf, np.array(waveDataArr), frqHz, sampwidth=3)
        # print('Saved to "' + strBuf + '"')

        ## ex2. Power spectrum (FFT)
        ## CAUTION: As the number of data increases, the calculation time increases. O(N log N)
        if len(self.soundDataArr) <= 262144:
            print('Start FFT...')
            n = len(self.soundDataArr)
            fftfreq = np.fft.fftfreq(n, 1/frqHz)
            fft = np.fft.fft(self.soundDataArr)/(n/2)
            fftmag = np.abs(fft)
            fftmag = 20 * np.log10(fftmag / P0_Pa)
            binIndexMaxPower = np.argmax(fftmag)
            maxmag = fftmag[binIndexMaxPower]
            freqMaxPower = binIndexMaxPower * (frqHz / n)
            print('Max: ' + '{:.3f}dB '.format(maxmag) + '@ {:.1f}Hz'.format(freqMaxPower))
            fig, panel = plt.subplots(1,1)
            panel.set_title('Power spectrum')
            panel.set_xlim(MIN_FRQ, fftfreq[:(int)(n/2)][-1])
            panel.set_ylim([0, 120])
            panel.set_xlabel('Frequency [Hz]')
            panel.set_ylabel('Sound [dB SPL]')
            panel.set_xscale('log')
            panel.plot(fftfreq[1:(int)(n/2)], fftmag[1:(int)(n/2)])
            strBuf = SAVE_DIR + self.filename + "_fft.png"
            fig.savefig(strBuf, dpi=300)
            print('Saved to "' + strBuf + '"')
            strBuf = SAVE_DIR + self.filename + '_fft.csv'
            np.savetxt(strBuf, np.array([fftfreq[1:(int)(n/2)], fftmag[1:(int)(n/2)]]).T, delimiter=',', header='Frequency [Hz],Sound [dB SPL]', comments='', fmt='%f')
            print('Saved to "' + strBuf + '"')


        ## ex3. Spectrogram (STFT)
        print('Start STFT...')
        frq, t, stft = scipy.signal.stft(self.soundDataArr, fs = frqHz, nperseg=4096)
        stftmag = 20 * np.log10(np.abs(stft) / P0_Pa)
        plt.figure(figsize=(12.0, 8.0))
        plt.pcolormesh(t, frq, stftmag, cmap = 'jet', vmin=0, vmax=120)
        plt.ylim(MIN_FRQ, frqHz/2)
        plt.title('Spectrogram')
        plt.ylabel('Frequency [Hz]')
        plt.xlabel('Time [sec]')
        plt.yscale('log')
        plt.colorbar()
        strBuf = SAVE_DIR + self.filename + "_spectrogram.png"
        plt.savefig(strBuf, dpi=300)
        print('Saved to "' + strBuf + '"')


        ## ex4. Scalogram (CWT)
        ## CAUTION: As the number of data increases, the calculation time increases. (recommend: <=16384)
        if len(self.soundDataArr) <= 16384:
            print('Start CWT...')
            w0 = 32     # tradeoff between time and frequency resolution
            t = np.linspace(0, 1/frqHz * len(self.soundDataArr), len(self.soundDataArr))
            scalogram = cwt(self.soundDataArr, 1/frqHz, w0, MIN_FRQ, frqHz/2, nf=100)
            fig = plt.figure(figsize=(12.0, 8.0))
            ax = fig.add_subplot(111)
            x, y = np.meshgrid(t, np.logspace(np.log10(MIN_FRQ), np.log10(frqHz/2), scalogram.shape[0]))
            scalogram_mag = 20 * np.log10(np.abs(scalogram) / P0_Pa)
            img = ax.pcolormesh(x, y, scalogram_mag, cmap = 'jet', vmin=0, vmax=120)
            ax.set_title("Scalogram")
            ax.set_xlabel("Time [sec]")
            ax.set_ylabel("Frequency [Hz]")
            ax.set_yscale('log')
            ax.set_ylim(MIN_FRQ, frqHz/2)
            plt.colorbar(img)
            strBuf = SAVE_DIR + self.filename + "_scalogram.png"
            fig.savefig(strBuf, dpi=300)
            print('Saved to "' + strBuf + '"')



    def __init__(self):
        self.filename = ''
        self.dataNum = 0
        self.last_packetNum = -1
        self.packetErrorNum = 0
        self.soundDataArr = array('f')
        print('Ready to receive')

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        jsonData = json.loads(message)
        # print(jsonData)
        
        if jsonData['packetNum'] == 0:
            ## Set filename and create directory
            dt_now = datetime.now()
            self.filename = dt_now.strftime('%Y%m%d%H%M%S')
            os.makedirs(SAVE_DIR, exist_ok=True)
        
        if self.filename != '':
            if jsonData['packetNum'] == 0:
                print('Start receiving data...')

            self.dataNum += len(jsonData['soundPressure'])

            ## Check packet loss and packet order
            if jsonData['packetNum'] != (self.last_packetNum + 1):
                self.packetErrorNum += 1
            self.last_packetNum = jsonData['packetNum']

            self.soundDataArr.extend(jsonData['soundPressure']) 

            if jsonData['final'] == True:
                print('Saving to file...')

                ## Offset cancellation(-50Pa)
                self.soundDataArr = [(n - 50.0) for n in self.soundDataArr]

                ## convert to string
                strWrite = '\n'.join(map(lambda n : '{:.4f}'.format(n), self.soundDataArr))

                p_file = pathlib.Path(SAVE_DIR + self.filename + '.txt')
                with p_file.open(mode='w') as f:
                    f.write(strWrite)
                print('Saved to "' + SAVE_DIR + self.filename + '.txt' + '", Number of data: ' + str(self.dataNum))
                
                self.myFunc(jsonData['frqHz'])

                if self.packetErrorNum > 0:
                    print('Packet loss may have occurred or the order of the packets may have been changed (' + str(self.packetErrorNum) + ')')
                self.__init__()


args = sys.argv
if len(args) == 1:
    portNum = 50000
    print('You can set port number as the 1st argument. Use default(50000)')
else:
    portNum = int(args[1])
    if not 1024 <= portNum <= 65535:
        print('Specifiled port number(' + str(portNum) + ') is out of range. Use default(50000)')
        portNum = 50000

queue = asyncio.Queue()
loop = asyncio.get_event_loop()
print("Starting UDP server(port:" + str(portNum) + ')' + '. Press Ctrl+C to quit')
listen = loop.create_datagram_endpoint(UdpServer, ('0.0.0.0', portNum))
transport, protocol = loop.run_until_complete(listen)

try:
    loop.run_forever()
except KeyboardInterrupt:
    print('UDP server has quit')
    pass

transport.close()
loop.close()
