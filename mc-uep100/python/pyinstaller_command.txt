```
pyinstaller saveSoundPressure_UdpSrv_GUI.py --onefile --clean --noconsole --collect-all obspy --distpath ./
```

Edit saveSoundPressure_UdpSrv_GUI.spec. Add below after `a = Analysis(...)`
```
for d in a.datas:
    if 'libevresp_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libgse2_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libmseed_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libsegy_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libsignal_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libtau_Windows_64bit_py39.cp39-win_amd64' in d[0]:
        a.datas.remove(d)
```

pyinstaller saveSoundPressure_UdpSrv_GUI.spec --distpath ./
