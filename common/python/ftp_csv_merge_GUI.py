############
# ftp_csv_merge_GUI.py v1.0
#
# Copyright (c) 2020 Y2Kb
#
# Released under the MIT license.
# see https://opensource.org/licenses/MIT
############
## Description: Merge each CSV files 'xxxxxxxx.csv' under the specified directory with the file name 'merge.csv' under the directory.
## Command example: 'python ftp_csv_merge.py
## Python version: 3.8.1~
## Pyinstaller command: pyinstaller ftp_csv_merge_GUI.py --onefile --noconsole --clean --distpath ./

import os
import glob
import sys
import csv
import datetime
import re


def main():
    # GUI
    import PySimpleGUI as sg
    sg.theme('Default1')
    layout = [
        [
            sg.Text('Merge Folder'),
            sg.InputText('', key='mergeFolderPath'),
            sg.FolderBrowse(),
        ],
        [sg.Text(''),],
        [
            sg.Submit(button_text='Merge', key='-submit-'),
            sg.Text('', key='-result-', size=(50,1)),
            sg.Button('Clear Log', key='-clearLog-'),
        ],
        [
            sg.Output(size=(65, 8), key='LogTextBox'),
        ]
    ]
    thisFileName = os.path.splitext(os.path.basename(os.path.basename(__file__)))[0]
    window = sg.Window(title=thisFileName).Layout(layout)

    while True:
        event, values = window.read()
        if event is None:
            break

        elif event in '-submit-':
            DATA_PATH = values['mergeFolderPath']
            print(f'Folder: "{os.path.abspath(DATA_PATH)}"')
            csv_files = [p for p in glob.glob(os.path.join(DATA_PATH, '**'), recursive=True) if re.fullmatch(r'\d{8}\.csv', os.path.basename(p))]
            if len(csv_files) == 0:
                print('Not found csv files.')
                continue
            csv_files.sort()

            output = os.path.join(DATA_PATH, 'merge.csv')
            with open(output, 'w') as outfile:
                for file in csv_files:
                    if os.path.basename(file) == '00000000.csv':
                        outfile.write('date,')
                    else:
                        outfile.write(datetime.datetime.fromtimestamp(os.path.getmtime(file)).isoformat() + ',')

                    with open(file, 'r') as infile:
                        line_num = 0
                        for line in infile:
                            outfile.write(line)
            print(f'Successfully merged to "{os.path.abspath(output)}".')

        elif event in '-clearLog-':
            window.FindElement('LogTextBox').Update('')
    window.close()
    sys.exit(0)


if __name__ == "__main__":
    main()
