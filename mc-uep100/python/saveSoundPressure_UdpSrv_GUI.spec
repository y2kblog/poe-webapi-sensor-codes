# -*- mode: python ; coding: utf-8 -*-
from PyInstaller.utils.hooks import collect_all

datas = []
binaries = []
hiddenimports = []
tmp_ret = collect_all('obspy')
datas += tmp_ret[0]; binaries += tmp_ret[1]; hiddenimports += tmp_ret[2]


block_cipher = None


a = Analysis(['saveSoundPressure_UdpSrv_GUI.py'],
             pathex=['C:\\ws\\project\\PoE_Sensor\\poe-webapi-sensor-codes\\mc-uep100\\python'],
             binaries=binaries,
             datas=datas,
             hiddenimports=hiddenimports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
for d in a.datas:
    if 'libevresp_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libgse2_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libmseed_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libsegy_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libsignal_Windows_64bit_py39.cp39-win_amd64' in d[0] or 'libtau_Windows_64bit_py39.cp39-win_amd64' in d[0]:
        a.datas.remove(d)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='saveSoundPressure_UdpSrv_GUI',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
