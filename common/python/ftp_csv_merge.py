############
# ftp_csv_merge.py v1.0
# 
# Copyright (c) 2020 Y2Kb
# 
# Released under the MIT license.
# see https://opensource.org/licenses/MIT
############
## Description: Merge each CSV files 'xxxxxxxx.csv' under the specified directory with the file name 'merge.csv' under the directory.
## Command example: 'python ftp_csv_merge.py C:/ftp/notify-dir/'
## 1st argument: Directory of CSV files (default: ./)
## Python version: 3.8.1~

import os
import glob
import sys
import csv
import datetime

DATA_PATH = "./"
args = sys.argv
if len(args) == 2:
    DATA_PATH = args[1]

csv_files = glob.glob(DATA_PATH + '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].csv')
if len(csv_files) == 0:
    print('Not found csv files.')
    sys.exit()
csv_files.sort()
# print(csv_files)

with open(DATA_PATH + 'merge.csv', 'w') as outfile:
    for file in csv_files:
        if os.path.basename(file) == '00000000.csv':
            outfile.write('date,')
        else:
            outfile.write(datetime.datetime.fromtimestamp(os.path.getmtime(file)).isoformat() + ',')

        with open(file, 'r') as infile:
            line_num = 0
            for line in infile:
                outfile.write(line)
